require 'rails_helper'

feature 'User answer', %q{
  In order to exchnage my knowledgr
  As an autheticated user
  I want to be able to create answers
} do
  
  let(:user) { create(:user) }
  let(:question) { create(:question)}
  scenario 'Authenticatrd user create anwer', js: true do
    sign_in(user)
    visit question_path(question)
    
    fill_in 'Your answer', with: 'My answer'
    click_on 'Create'
    
    expect(current_path).to eq question_path(question)
    #save_and_open_page
    within '.answers' do
      expect(page).to have_content ""
    end
  end
end