require 'rails_helper'

feature 'Create question', %q{
  In order to get answer from community
  As an authenticated user
  I want to be able to ask the question
} do

  #given(:user) { create(:user) }

  scenario 'Authenticated user create the question' do
    sign_in(User.create!(email: 'user@test.com', password: '12345678'))
    
    visit questions_path
    click_on 'Ask question'
    fill_in 'Title', with: 'Test question'
    fill_in 'Body', with: 'test text'
    click_on 'Create'
    expect(page).to have_content ""
  end

  scenario 'Non-unauthenticated user ries to create question' do
    visit questions_path    
    click_on 'Ask question'
    #save_and_open_page
    expect(page).to have_content "You need to sign in or sign up before continuing."
  end
end

